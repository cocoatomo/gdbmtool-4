#!/bin/env python
# -*- coding: utf-8 -*-

import sys
import argparse
import base64
import gdbm

def gdbm_load_from_file(db, fp):
    flag = False
    key = None
    value = None
    for line in fp:
        if flag:
            value = base64.b64decode(line)
            db[key] = value
            key = None
            value = None
        else:
            key = base64.b64decode(line)

        flag ^= True

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--mode", type=int, help="Set database file mode (octal number)")
    parser.add_argument("FILE")
    parser.add_argument("DB_FILE")

    args = parser.parse_args()

    filename = args.FILE
    dbname = args.DB_FILE

    fp = None
    if filename == "-":
        fp = sys.stdin
    else:
        fp = open(filename, "r")

    db = None
    if args.mode is not None:
        db = gdbm.open(dbname, "c", args.mode)
    else:
        db = gdbm.open(dbname, "c")

    gdbm_load_from_file(db, fp)

    if db is not None:
        db.close()

    return 0

if __name__ == "__main__":
    sys.exit(main())

